import argparse
import sys
import os
import time
import logging
from miscoto import query, sbml, commons, utils
from os import listdir
from os.path import isfile, join
from clyngor.as_pyasp import TermSet, Atom
from xml.etree.ElementTree import ParseError
import clyngor

logger = logging.getLogger(__name__)

def extract_pred(model, predic) :
    return list(a[0] for pred in model if pred == predic for a in model[pred])

def extract_score(model) :
    return list(a for pred in model if pred == 'scramblescore' for a in model[pred])

def run_competition(instance_lp, encoding,
                intersection=False, enumeration=False, union=False, optsol=False) :
    # optimal solutions
    prg = [instance_lp, encoding]
    if optsol:
        logger.info('\n*** OPTIMAL SOLUTIONS ***')
        models = query.get_communities_competition(instance_lp, encoding)
        # models = clyngor.solve(prg, options = '--project', clingo_bin_path='asprin', use_clingo_module=False, nb_model=0).with_optimality.by_predicate
        # score = one_model[1]
        # optimum = ','.join(map(str, score))
        # one_model = one_model[0]
        still_unprod = {}
        bacteria = {}
        newly_prod = {}
        lst_exchanged = {}
        all_models = {}
        results = {}
        competitionpop = {}
        competitionindiv ={}
        num_model = 1
        print(models)
        for idx, model in enumerate(models, start=1):
            exchanged = {}
            if model[-1] == True:
                one_model = model[0]
                still_unprod[num_model] = extract_pred(one_model, 'unproducible_target')
                newly_prod[num_model] = extract_pred(one_model, 'newly_producible_target')
                bacteria[num_model] = extract_pred(one_model, 'chosen_bacteria')
                scramblescore = extract_score(one_model)
                for pred in one_model : 
                    if pred == 'exchanged':
                        for a in one_model[pred]:
                            if (a[2], a[3]) in exchanged:
                                exchanged[(a[2], a[3])].append(a[0])
                            else:
                                exchanged[(a[2], a[3])] = []
                                exchanged[(a[2], a[3])].append(a[0])
                lst_exchanged[num_model] = exchanged
                logger.info(' ***  optimal model '+str(num_model)+'  ***')
                logger.info(str(len(newly_prod[num_model])) + ' newly producible target(s):')
                logger.info("\n".join(newly_prod[num_model]).replace("\"", ""))
                logger.info('Still ' + str(len(still_unprod[num_model])) + ' unproducible target(s):')
                logger.info("\n".join(still_unprod[num_model]))
                logger.info('Minimal set of bacteria of size ' + str(len(bacteria[num_model])))
                logger.info("\n".join(bacteria[num_model]).replace("\"", ""))
                if len(exchanged) >= 1:
                    logger.info('Minimal set of exchanges of size => ' +
                                str(sum(len(v) for v in exchanged.values())))
                    for fromto in exchanged:
                        logger.info("\texchange(s) from " + fromto[0].replace("\"", "") + ' to ' +
                                    fromto[1].replace("\"", "") + " = " + ','.join(exchanged[fromto]).replace("\"", ""))
                dicoScore = {}
                for score in scramblescore : 
                    dicoScore[score[0]] = int(score[1])
                competitionindiv[num_model] = dicoScore
                Sum = 0
                if len(dicoScore) > 0:
                    logger.info('scramble score for each bacterium is:')
                    for key in dicoScore :
                        Sum += dicoScore[key]
                        logger.info("\tindividual scramble competiton for " +key.replace('\"', '') + 
                                    " = " + (str(dicoScore[key])))
                competitionpop[num_model] = Sum/(len(bacteria[num_model])+1)
                dicoCome = {}
                for come in AllCome : 
                    dicoCome[come[0]] = int(come[1])
                SumSSmax = 0
                if len(dicoCome) > 0:
                    for key in dicoCome :
                        SumSSmax += dicoCome[key]*100
                SSmaxpop = (SumSSmax/(len(bacteria[num_model])+1))
                SSpop = ((sum(dicoScore.values())/(len(bacteria[num_model])+1)))
                competitionpercentage[num_model] = SSpop*100/SSmaxpop
                logger.info('percentage of competition for the population is '+str(round(competitionpercentage[num_model]))+" %")
                num_model = num_model+1
                logger.info("")
                all_models[num_model] = one_model

        results['models'] = all_models
        results['exchanged'] = lst_exchanged
        results['bacteria'] = bacteria
        results['still_unprod'] = still_unprod
        results['newly_prod'] = newly_prod
        results['competition_population'] = competitionpop
        results['competition_individual'] = competitionindiv

    # union of solutions
    if union:
        logger.info('\n*** UNION OF MINIMAL SOLUTION ***')
        try:
            if optsol:
                union_m = query.get_union_communities_from_g(grounded_instance, optimum)
            else:
                union_m = query.get_union_communities_from_g_noopti(grounded_instance)
        except IndexError:
            logger.error(
                "No stable model was found. Possible troubleshooting: no harmony between names for identical metabolites among host and microbes"
            )
            quit()
        union_score = union_m[1]
        optimum_union = ','.join(map(str, union_score))
        union_m = union_m[0]
        union_bacteria = []
        union_exchanged = {}
        for pred in union_m :
            if pred == 'chosen_bacteria':
                for a in union_m[pred, 1]:
                    union_bacteria.append(a[0])
            elif pred == 'exchanged':
                for a in union_m[pred, 4]:
                    if (a[2], a[3]) in union_exchanged:  #union_exchanged[(from,to)]=[(what,compartto);(what,compartto)]
                        union_exchanged[(a[2], a[3])].append(a[0])
                    else:
                        union_exchanged[(a[2], a[3])] = []
                        union_exchanged[(a[2], a[3])].append( a[0])
        logger.info('Union of minimal sets of bacteria, with optimum = ' +
                    optimum_union + ' comprises ' + str(len(union_bacteria)) +
                    ' bacteria')
        logger.info("\n".join(union_bacteria))
        if len(union_exchanged) >= 1:
            logger.info('\nExchanges in union => ' +
                        str(sum(len(v) for v in union_exchanged.values())))
            for fromto in union_exchanged:
                logger.info('\texchange(s) from ' + fromto[0] + ' to ' +
                            fromto[1] + " = " +
                            ','.join(union_exchanged[fromto]))
        results['union_exchanged'] = union_exchanged
        results['union_bacteria'] = union_bacteria
        results['optimum_union'] = optimum_union
# intersection of solutions
    if intersection:
        logger.info('\n*** INTERSECTION OF MINIMAL SOLUTION ***')
        if optsol:
            intersection_m = query.get_intersection_communities_from_g(grounded_instance, optimum)
        else:
            intersection_m = query.get_intersection_communities_from_g_noopti(grounded_instance)
        intersection_score = intersection_m[1]
        optimum_inter = ','.join(map(str, intersection_score))
        intersection_m = intersection_m[0]
        inter_bacteria = []
        inter_exchanged = {}
        for pred in intersection_m :
            if pred == 'chosen_bacteria':
                for a in intersection_m[pred, 1]:
                    inter_bacteria.append(a[0])
            elif pred == 'exchanged':
                for a in intersection_m[pred, 4]:
                    if (a[2], a[3]) in inter_exchanged:  #inter_exchanged[(from,to)]=[(what,compartto);(what,compartto)]
                        inter_exchanged[(a[2], a[3])].append(a[0])
                    else:
                        inter_exchanged[(a[2], a[3])] = []
                        inter_exchanged[(a[2], a[3])].append(a[0])
        logger.info('Intersection of minimal sets of bacteria, with optimum = '
                    + optimum_inter + ' comprises ' +
                    str(len(inter_bacteria)) + ' bacteria')
        logger.info("\n".join(inter_bacteria))
        if len(inter_exchanged) >= 1:
            logger.info('\nExchanges in intersection => ' +
                        str(sum(len(v) for v in inter_exchanged.values())))
            for fromto in inter_exchanged:
                logger.info('\texchange(s) from ' + fromto[0] + ' to ' +
                            fromto[1] + " = " +
                            ','.join(inter_exchanged[fromto]))
        results['inter_exchanged'] = inter_exchanged
        results['inter_bacteria'] = inter_bacteria
        results['optimum_inter'] = optimum_inter

# enumeration of all solutions
    if enumeration:
        logger.info('\n*** ENUMERATION OF MINIMAL SOLUTION ***')
        if optsol:
            all_models = query.get_all_communities_from_g(grounded_instance, optimum)
        else:
            all_models = query.get_all_communities_from_g_noopti(grounded_instance)
        count = 1

        results['enum_bacteria']  = {}
        results['enum_exchanged'] = {}
        for model in all_models:
            enum_bacteria_this_sol = []
            enum_exchanged_this_sol = {}
            logger.info('\nSolution ' + str(count))
            for pred in model:
                if pred == 'chosen_bacteria':
                    for a in model[pred, 1]:
                        enum_bacteria_this_sol.append(a[0])
                elif pred == 'exchanged':
                    for a in model[pred, 4]:
                        if (a[2], a[3]) in enum_exchanged_this_sol:  #enum_exchanged_this_sol[(from,to)]=[(what,compartto);(what,compartto)]
                            enum_exchanged_this_sol[(a[2], a[3])].append(a[0])
                        else:
                            enum_exchanged_this_sol[(a[2], a[3])] = []
                            enum_exchanged_this_sol[(a[2], a[3])].append(a[0])
            logger.info("\t" + str(len(enum_bacteria_this_sol)) +
                        " bacterium(ia) in solution " + str(count))
            for elem in enum_bacteria_this_sol:
                logger.info("\t" + elem)
            if len(enum_exchanged_this_sol) >= 1:
                logger.info("\t" +
                            str(sum(len(v) for v in enum_exchanged_this_sol.values())) +
                            " exchange(s) in solution " + str(count))
                for fromto in enum_exchanged_this_sol:
                    logger.info('\texchange(s) from ' + fromto[0] + ' to ' +
                                fromto[1] + " = " +
                                ','.join(enum_exchanged_this_sol[fromto]))
            results['enum_exchanged'][count] = enum_exchanged_this_sol
            results['enum_bacteria'][count] = enum_bacteria_this_sol
            count+=1
    
    return results
