from miscoto.miscoto_instance import run_instance
from miscoto.miscoto_mincom import run_mincom
from miscoto.miscoto_scopes import run_scopes


__version__="2.0.7"
"""Define the version of the package.
"""